/*jshint esversion: 6*/
/*jslint devel: true*/
/*jslint nomen: true*/
/*eslint-env browser*/
"use strict";

// Globs
var MOVE_TIMEOUT_MS = 250, _videoTO, _startOriginal, _video, _modal, _user, _canvas, _ctx, _z_canvas, _game, _ul_timer, _ul_munitions, _ul_score, _ul_level, _f_standard, _f_big, _f_reduced, _db_alain, _db_chuck, _ft_alain, _ft_chuck, _hallOfFame;

// Ajouter ici les scores "ennemis"
const enemyScores = JSON.stringify([{username:"MayaAmano", score:3690, city:"Tokyo"},{username:"Fantasio", score:1420, city:"Champignac"},{username:"LarryLaChance", score:6900, city:"Vladivostok"}]);


// Geolocation
function onSuccess(position) {
    let crd = position.coords;
    console.log("Bonjour ! Voici votre position actuelle :");
    console.log(`Latitude : ${crd.latitude}`);
    console.log(`Longitude: ${crd.longitude}`);
    
    /* Obtenir la ville à partir de la latitude et longitude, utiliser API : https://locationiq.com/*/
    let settings = {
        "async": true,
        "crossDomain": true,
        "url": `https://eu1.locationiq.com/v1/reverse.php?key=pk.ddea73669bc2468542a89a5ed867698e&lat=${crd.latitude}&lon=${crd.longitude}&format=json`,
        "method": "GET"
    }
    
    $.ajax(settings).done(function(response){
        if(typeof (response.address.town) === 'undefined'){
            _user.city = response.address.city;
        } else{
            _user.city = response.address.town;
        } 
        console.log(`Vous habitez à ${_user.city} :)`);
        console.log(`... Avec une précision de ${crd.accuracy} mètres.`);
    }); 
    
}

// Basic objects
function Point(x, y) {
	this.x = x;
	this.y = y;
    this.toString = function () {
        return "(" + x + "," + y + ")";
    };
}
function Size(w, h) {
    this.w = w;
    this.h = h;
    this.toString = function () {
        return this.w + ";" + this.h;
    };
}
function Rectangle(x, y, w, h) {
	this.location = new Point(x, y);
    this.size = new Size(w, h);
    this.top = function () {
        return this.location.y;
    };
    this.bottom = function () {
        return this.location.y + this.size.h;
    };
    this.left = function () {
        return this.location.x;
    };
    this.right = function () {
        return this.location.x + this.size.w;
    };
    this.contains = function (point) {
        return point.x >= this.location.x && point.x < this.location.x + this.size.w && point.y >= this.location.y && point.y < this.location.y + this.size.h;
    };
    this.crosses = function (rectangle) {
        return !((this.right() < rectangle.left()) || (this.left() > rectangle.right()) || (this.top() > rectangle.bottom()) || (this.bottom() < rectangle.top()));
    };
    this.toString = function () {
        return "Location: " + this.location.toString() + "\r\nSize: " + this.size.toString();
    };
}
function Font(family, size) {
    this.family = family;
    this.size = size;
    this.toString = function () {
        return this.size + "px " + this.family;
    };
}
function User(username) {
    this.username = username;
    this.score = 0;
}

// Basic functions
function startGame() {
    _game.start();
}
function showModal() {
    _modal.style.display = "block";
}
function finalCountdown(seconds){
    // Hide video
    document.getElementById("vid").style.display = "none";
    document.getElementById("skipbtn").style.display = "none";

    // Show canvas
    document.getElementById("piscine").style.display = "block"

    let pastSeconds = 0, ul_cntdown = new UpdatableLabel(seconds.toString(10), new Font("Arial", 28), false, new Point(450, 286));

    let x = setInterval(function(){
        let s = seconds - pastSeconds;
        // If there are seconds remaining we show them, else we empty the <p> tag
        if(s > 0) {
            ul_cntdown.setValue(s.toString(10));
        }
        else if (s === 0) {
            ul_cntdown.setValue("GO");
        }
        else {
            clearInterval(x);
            ul_cntdown.setValue("");
        }
        pastSeconds++;            
    }, 1000);
}
function closeModal() {
    _user = new User(document.getElementById("username").value);
    
    /* Géolocalisation */
    let geoloc = navigator.geolocation;
    if(geoloc){
        geoloc.getCurrentPosition(onSuccess);
    }

    _video = document.getElementById("vid");
    let source = document.createElement("source");
    source.setAttribute("src", "ressources/videos/introduction.mp4");
    source.setAttribute("type", "video/mp4");

    _video.appendChild(source);
    _video.setAttribute("width", "350px");
    _video.style.display = "block";
    _video.style.marginLeft = "auto";
    _video.style.marginRight = "auto";

    _video.play();

    _modal.style.display = "none";
    // timer avant départ du jeu
    _videoTO = setTimeout(function(){finalCountdown(3);}, 44000);
    _startOriginal = setTimeout(function () {
        startGame();
    }, 49000);
}



function hideHallOfFame() {
    _hallOfFame.style.display = "none";
}
function showHallOfFame() {
    _user.score = _ul_score.value;
    let userToSave = {username:_user.username, score:_user.score, city:_user.city}
    _hallOfFame.style.display = "block";
    let position = 1;
    let table = document.getElementById("results");
    
    if(!localStorage.getItem('highscore')){
        localStorage.setItem('highscore', enemyScores);
    }
    const savedScores = localStorage.getItem('highscore') // get the scores

    const highscores = [...JSON.parse(savedScores), userToSave] // add the result
    .sort((a, b) => b.score- a.score) // sort descending
    .slice(0, 6) // take highest 6

    localStorage.setItem('highscore', JSON.stringify(highscores)) // store the scores
    
    let retrievedScores = JSON.parse(window.localStorage.getItem('highscore'))
    
    for (let i = 0; i < retrievedScores.length; i++) {
        table.innerHTML += '<tr><td>' + position + '</td><td>' + retrievedScores[i].username + '</td><td>' + retrievedScores[i].score + '</td><td>' + retrievedScores[i].city + '</td></tr>'
        position+= 1;
    }

    document.getElementById("exitGame").addEventListener('click', function () {
        hideHallOfFame();
    });
}
function getRandomBetween(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}
function getTextWidth(text, font) {
	let fontSave = _ctx.font, rv;
	_ctx.font = font.toString();
	rv = _ctx.measureText(text).width;
	_ctx.font = fontSave;
	return rv;
}
function underlineText(textWidth, x, y) {
	_ctx.rect(x, y + 2, textWidth, 1);
	_ctx.stroke();
}
function getCursorPosition(event) {
	let rect = _canvas.getBoundingClientRect(), location = new Point(event.clientX - rect.left, event.clientY - rect.top), shotgun = null;
    if (_game.playing) {
        if (_ul_munitions.value > 0) {
            if (location.y < 300) {
                shotgun = new window.Audio('ressources/audio/shotgun_far.mp3');
            } else {
                shotgun = new window.Audio('ressources/audio/shotgun_close.mp3');
            }
            _game.shoot(location);
            _ul_munitions.setValue(_ul_munitions.value - 1);
        } else {
            shotgun = new window.Audio('ressources/audio/shotgun_empty.mp3');
        }
        shotgun.play();
    }
}

// Complex objects
function FixedTarget(location, size, value, imgsrc) {
    this.rectangle = new Rectangle(location.x, location.y, size.w, size.h);
    this.value = value;
    this.imgsrc = imgsrc;
    this.visible = true;
    this.containsPoint = function (point) {
        return this.rectangle.contains(point);
    };
    this.draw = function () {
        _ctx.clearRect(this.rectangle.location.x, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
        if (this.visible) {
            let img = new Image(), rectangle = this.rectangle;
            img.src = imgsrc;
            img.onload = function () {
                _ctx.drawImage(img, rectangle.location.x, rectangle.location.y, rectangle.size.w, rectangle.size.h);
                _ctx.stroke();
            };
        }
    };
}
function MobileTarget(lane, size, value, imgsrc, speed, toLeft) {
    this.lane = lane;
    this.rectangle = new Rectangle(Math.round(Math.random() * lane.rectangle.size.w), lane.rectangle.location.y + 15, size.w, size.h);
    this.imgsrc = imgsrc;
    this.speed = speed;
    this.toLeft = toLeft;
    this.visible = true;
    this.value = value;
    this.setLane = function (lane) {
        this.lane = lane;
        this.rectangle = new Rectangle(Math.round(Math.random() * lane.rectangle.size.w), lane.rectangle.location.y + 15, this.rectangle.size.w, this.rectangle.size.h);
    };
    this.setVisible = function (visible) {
        this.visible = visible;
    };
    this.setToLeft = function (toLeft) {
        this.toLeft = toLeft;
    };
    this.crossesItem = function (item) {
        return this.rectangle.crosses(item.rectangle);
    };
    this.containsPoint = function (point) {
        return this.rectangle.contains(point);
    };
    this.move = function () {
        let newx;
        if (this.toLeft) {
            newx = this.rectangle.location.x - this.speed;
            if (this.rectangle.location.x <= this.lane.rectangle.location.x) {
                newx = this.lane.rectangle.size.w;
            }
        } else {
            newx = this.rectangle.location.x + this.speed;
            if (this.rectangle.location.x >= this.lane.rectangle.size.w) {
                newx = this.lane.rectangle.location.x - this.rectangle.size.w;
            }
        }
        this.rectangle = new Rectangle(newx, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
    };
    this.draw = function () {
//        _ctx.clearRect(this.rectangle.location.x, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
        if (this.visible) {
            let img = new Image(), rectangle = this.rectangle;
            img.src = imgsrc;
            img.onload = function () {
                _ctx.drawImage(img, rectangle.location.x, rectangle.location.y, rectangle.size.w, rectangle.size.h);
                _ctx.stroke();
            };
        }
    };
}
function Duck(lane) {
    let toLeft = Math.round(Math.random()) === 1, lr = toLeft ? 'l_' : 'r_', imgsrc = 'ressources/images/duck_' + lr + Math.round(Math.random()) + '.png';
    return new MobileTarget(lane, new Size(55, 55), 100, imgsrc, 20, toLeft);
}
function Shark(lane) {
    let toLeft = Math.round(Math.random()) === 1, lr = toLeft ? 'l_0.png' : 'r_0.png', imgsrc = 'ressources/images/shark_' + lr;
    return new MobileTarget(lane, new Size(75, 55), 250, imgsrc, 30, toLeft);
}
function Zone(rectangle) {
    this.rectangle = rectangle;
    this.visible = true;
    this.imgsrc = "";
    this.items = [];
    this.interval = null;
    this.containsPoint = function (point) {
        return this.rectangle.contains(point);
    };
    this.addItem = function (item) {
        this.items.push(item);
    };
    this.removeItem = function (item) {
        let idx = this.items.indexOf(item);
        if (idx > -1) {
            return this.items.splice(idx, 1);
        }
    };
    this.clearItems = function(){
        this.items = [];
    }
    this.startMoveItems = function () {
        let zoneInstance = this;
        this.interval = setInterval(function () {
            zoneInstance.moveItems();
        }, MOVE_TIMEOUT_MS);
    };
    this.moveItems = function () {
        this.items.forEach(function (item) {
            if (item instanceof Zone) {
                item.moveItems();
            } else if (item instanceof MobileTarget) {
                item.move();
            }
        });
        this.draw();
    };
    this.stopItems = function () {
        if (this.interval !== null) {
            clearInterval(this.interval);
        }
    };
    this.setVisible = function (visible) {
        this.visible = visible;
    };
    this.setImgsrc = function (imgsrc) {
        this.imgsrc = imgsrc;
    };
    this.draw = function () {
//        _ctx.clearRect(this.rectangle.location.x, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
        if (this.visible) {
            if (this.imgsrc !== "") {
                let img = new Image();
                img.src = this.imgsrc;
                img.onload = function () {
                    _ctx.drawImage(img, rectangle.location.x, rectangle.location.y, rectangle.size.w, rectangle.size.h);
                    _ctx.stroke();
                };
            }
            this.items.forEach(function (item) {
                item.draw();
            });
        }
    };
}
function UpdatableLabel(value, font, alignRight, origin) {
	this.value = value;
	this.location = origin;
	this.font = font;
	this.alignRight = alignRight;
    this.visible = true;
    this.rectangle = new Rectangle(this.location.x, this.location.y, 0, 0);
    this.containsPoint = function (point) {
        return this.rectangle.contains(point);
    };
    this.setValue = function (value) {
        this.value = value;
        this.draw();
    };
    this.setVisible = function (visible) {
        this.visible = visible;
        this.draw();
    };
	this.draw = function () {
		_ctx.font = this.font.toString();
		_ctx.textAlign = this.alignRight ? "right" : "left";
        _ctx.clearRect(this.rectangle.location.x, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
        let textw = getTextWidth(this.value, this.font.toString());
        if (this.alignRight) {
            textw = -textw;
        }
        this.rectangle = new Rectangle(this.location.x, this.location.y, textw, -20);
		if (this.visible) {
            _ctx.fillText(this.value, this.location.x, this.location.y);
        }
	};
}
function DialogBox(title, titleFont, text, textFont, toRight, origin) {
	this.title = title;
    this.titleFont = titleFont;
	this.text = text;
    this.textFont = textFont;
	this.toRight = toRight;
	this.location = origin;
    this.visible = true;
    this.rectangle = new Rectangle(this.location.x, this.location.y, 0, 0);
    this.containsPoint = function (point) {
        return this.rectangle.contains(point);
    };
    this.setTitle = function (title) {
        this.title = title;
        this.draw();
    };
    this.setText = function (text) {
        this.text = text;
        this.draw();
    };
    this.setVisible = function (visible) {
        this.visible = visible;
        this.draw();
    };
	this.draw = function () {
		let textw = getTextWidth(this.text, this.textFont), titlew = getTextWidth(this.title, this.titleFont), largestw = titlew > textw ? titlew : textw, locx, titleloc, textloc, steps;
        locx = this.location.x - largestw - 20;
        if (this.toRight) {
            locx = this.location.x + 20;
		}
        titleloc = new Point(locx, this.location.y + 20);
        textloc = new Point(locx, this.location.y + 50);
        _ctx.beginPath();
        _ctx.clearRect(this.rectangle.location.x, this.rectangle.location.y, this.rectangle.size.w, this.rectangle.size.h);
		if (this.visible) {
            _ctx.font = this.textFont.toString();
            _ctx.textAlign = "left";
            _ctx.fillText(this.text, textloc.x, textloc.y);
            _ctx.font = this.titleFont.toString();
            _ctx.fillText(this.title, titleloc.x, titleloc.y);
            underlineText(titlew, titleloc.x, titleloc.y);
            _ctx.moveTo(this.location.x, this.location.y);
            steps = [ this.location.x - largestw - 30, this.location.x - 10, -largestw - 32 ];
            if (this.toRight) {
                steps = [ this.location.x + largestw + 30, this.location.x + 10, largestw + 32 ];
            }
            _ctx.lineTo(steps[0], this.location.y);
            _ctx.lineTo(steps[0], this.location.y + 60);
            _ctx.lineTo(steps[1], this.location.y + 60);
            _ctx.lineTo(steps[1], this.location.y + 20);
            this.rectangle = new Rectangle(this.location.x - 1, this.location.y - 1, steps[2], 62);
            _ctx.lineTo(this.location.x, this.location.y);
        }
        _ctx.closePath();
		_ctx.stroke();
	};
}
function Level(lanes, gameZone) {
    this.lanes = lanes;
    this.gameZone = gameZone;
    this.sharks = [];
    this.ducks = [];
    this.started = false;
    this.finished = false;
    this.addDuckTime = [];
    this.addSharkTime = [];
    this.switchLanesTime = [];
    this.interval = null;
    this.ammo = 5;
    this.timer = 15;
    this.nbDucks = 3;
    this.nbSharks = 1;
    this.nbSwitchLanes = 0;
    this.setAmmo = function (ammo) {
        this.ammo = ammo;
    };
    this.setTimer = function (timer) {
        this.timer = timer;
    };
    this.getTimer = function(){
        return this.timer;
    }
    this.setNbDucks = function (nbDucks) {
        this.nbDucks = nbDucks;
    };
    this.setNbSharks = function (nbSharks) {
        this.nbSharks = nbSharks;
    };
    this.setNbSwitchLanes = function (nbSwitchLanes) {
        this.nbSwitchLanes = nbSwitchLanes;
    };
    this.addDuck = function () {
        let lane = getRandomBetween(0, 4), duck = new Duck(this.lanes[lane]);
        this.ducks.push(duck);
        this.lanes[lane].addItem(duck);
    };
    this.removeDuck = function (duck) {
        let lane = duck.lane, idx = this.ducks.indexOf(duck);
        lane.removeItem(duck);
        return this.ducks.splice(idx, 1);
    };
    this.addShark = function () {
        let lane = getRandomBetween(0, 4), shark = new Shark(this.lanes[lane]);
        this.sharks.push(shark);
        this.lanes[lane].addItem(shark);
    };
    this.removeShark = function (shark) {
        let lane = shark.lane, idx = this.sharks.indexOf(shark);
        lane.removeItem(shark);
        return this.sharks.splice(idx, 1);
    };
    
    this.removeSharks = function(){
        this.sharks = [];
    }
    
    this.moveSharksToFullLane = function (levelInstance) {
        levelInstance.sharks.forEach(function (shark) {
            let i = levelInstance.lanes.indexOf(shark.lane), switchLane = false, hasDucks;
            hasDucks = function (r) {
                return levelInstance.ducks.includes(r);
            };
            while (!levelInstance.lanes[i].items.some(hasDucks)) {
                switchLane = true;
                i = getRandomBetween(0, 4);
            }
            if (switchLane) {
                levelInstance.switchLanes(shark, levelInstance.lanes[i]);
            }
        });
    };
    this.addDucks = function (levelInstance) {
        let ducks = levelInstance.nbDucks > 5 ? getRandomBetween(5, levelInstance.nbDucks) : levelInstance.nbDucks;
        this.nbDucks -= ducks;
        while (ducks > 0) {
            levelInstance.addDuck();
            ducks -= 1;
        }
    };
    this.addSharks = function (levelInstance) {
        let sharks = levelInstance.nbSharks > 1 ? getRandomBetween(1, levelInstance.nbSharks) : levelInstance.nbSharks;
        this.nbSharks -= sharks;
        while (sharks > 0) {
            levelInstance.addShark();
            sharks -= 1;
        }
    };
    this.switchLanes = function (item, newlane) {
        let currlane = item.lane;
        currlane.removeItem(item);
        item.setVisible(false);
        item.setLane(newlane);
        newlane.addItem(item);
        setTimeout(function () {
            item.setVisible(true);
        }, getRandomBetween(200, 600));
    };
    this.checkForSharksKill = function (levelInstance) {
        if (levelInstance.sharks.length > 0) {
            levelInstance.sharks.forEach(function (shark) {
                levelInstance.ducks.forEach(function (duck) {
                    if (shark.crossesItem(duck)) {
                        levelInstance.removeDuck(duck);
                    }
                });
            });
        }
    };
    this.initTimers = function () {
        let ducks = this.nbDucks, sharks = this.nbSharks; //this.nbDucks % 5, sharks = this.nbSharks % 10;
        //this.addDuckTime = [];
        while (ducks > 0) {
            this.addDuckTime.push(getRandomBetween(10, this.timer -1));
            ducks--;
        }
        //this.addSharkTime = [];
        while (sharks > 0) {
            this.addSharkTime.push(getRandomBetween(10, this.timer -1));
            sharks --;
        }
        //this.switchLanesTime = [];
        while (this.nbSwitchLanes > 0) {
            this.switchLanesTime.push(getRandomBetween(10, this.timer -1));
            this.nbSwitchLanes--;
        }
    };
    this.getInterval = function (gameInstance) {
        let levelInstance = this;
        return setInterval(function () {
            levelInstance.timer -= 1;
            if (_ul_munitions.value === 0) {
                levelInstance.timer = 0;
            }
            if (levelInstance.ducks.length === 0) {
                if (levelInstance.nbDucks === 0) {
                    // Remove every remaining shark
                    if(levelInstance.sharks.length > 0){
                        /*let i = 0;
                        while(i < levelInstance.sharks.length){
                            levelInstance.removeShark(levelInstance.sharks[0]);
                            i++;
                        }*/
                        levelInstance.removeSharks();
                        for(let i = 0; i < levelInstance.lanes.length; i++){
                            levelInstance.lanes[i].clearItems();
                        }
                    }

                    levelInstance.finished = true;
                    levelInstance.timer = 0;

                } else { 
                    levelInstance.addDucks(levelInstance);
                }
            }
            if (levelInstance.timer <= 0) {
                levelInstance.gameZone.stopItems();
                gameInstance.nextLevel();
                levelInstance.clearInterval(gameInstance);
            } else {
                levelInstance.moveSharksToFullLane(levelInstance);
                levelInstance.checkForSharksKill(levelInstance);
                if (levelInstance.addDuckTime.includes(levelInstance.timer)) {
                    levelInstance.addDucks(levelInstance);
                }
                if (levelInstance.addSharkTime.includes(levelInstance.timer)) {
                    levelInstance.addSharks(levelInstance);
                }
                if (levelInstance.switchLanesTime.includes(levelInstance.timer)) {
                    levelInstance.ducks.forEach(function (item) {
                    if (Math.round(Math.random()) === 1) {
                            levelInstance.switchLanes(item, levelInstance.lanes[Math.round(Math.random() * 4)]);
                        }
                    });
                }
            }
            _ul_timer.setValue(levelInstance.getTimer());
        }, 1000);
    };
    this.clearInterval = function () {
        clearInterval(this.interval);
    };
    this.start = function (gameInstance) {
        let initducks;

        if(this.nbDucks < 5)
            initducks = this.nbDucks;
        else if(this.nbDucks > 10)
            initducks = getRandomBetween(5, 10);
        else
            initducks = getRandomBetween(5, this.nbDucks);

        this.started = true;
        _ul_level.setValue(_ul_level.value + 1);
        _ul_munitions.setValue(this.ammo);
        this.nbDucks -= initducks;
        this.initTimers();
        do {
            this.addDuck();
            initducks--;
        } while (initducks > 0);
        this.gameZone.startMoveItems();
        this.interval = this.getInterval(gameInstance);
    };
    this.shoot = function (point) {
        let shotDucks = [], shotSharks = [];
        this.ducks.forEach(function (duck) {
            if (duck.containsPoint(point)) {
                let randomNumber = getRandomBetween(1, 2);
                let soundFile;
                switch(randomNumber){
                    case 1:
                    soundFile = 'ressources/audio/euh.mp3';
                    break;

                    case 2:
                        soundFile = 'ressources/audio/Igh.mp3';
                        break;

                    default:
                        soundFile = 'ressources/audio/Igh.mp3';
                }

                new window.Audio(soundFile).play();
                shotDucks.push(duck);
            }
        });
        this.sharks.forEach(function (shark) {
            if (shark.containsPoint(point)) {
                shotSharks.push(shark);
            }
        });
        if (shotDucks.length === 0 && shotSharks.length === 0) {
            if (_ft_alain.containsPoint(point)) {
                _ul_score.setValue(_ul_score.value - 100);
                _db_alain.setText("Me tirez pas dessus !");
                setTimeout(function () {
                    _db_alain.setText("Sauvez-moi !");
                }, 2000);
            } else if (_ft_chuck.containsPoint(point)) {
                _ul_score.setValue(0);
                _ul_munitions.setValue(1);
                _db_chuck.setText("No one shoots Chuck Norris. Chuck Norris shoots you.");
            } else if (_ul_score.value >= 10) {
                _ul_score.setValue(_ul_score.value - 10);
            }
        } else {
            shotDucks.forEach(function (duck) {
                _ul_score.setValue(_ul_score.value + duck.value);
                this.removeDuck(duck);
            }, this);
            shotSharks.forEach(function (shark) {
                _ul_score.setValue(_ul_score.value + shark.value);
                this.removeShark(shark);
            }, this);
        }
    };
}
function Game() {
    this.levels = [];
    this.lanes = [];
    this.playing = false;
    this.start = function () {
        let i = 0, z_top = new Zone(new Rectangle(0, 0, _z_canvas.rectangle.size.w, 90)), z_pool = new Zone(new Rectangle(0, z_top.rectangle.size.h, _z_canvas.rectangle.size.w, 425)), z_bottom = new Zone(new Rectangle(0, z_top.rectangle.size.h + z_pool.rectangle.size.h, _z_canvas.rectangle.size.w, 85)), l_munitions = new UpdatableLabel("MUNITIONS", _f_standard, false, new Point(10, 30)), l_score = new UpdatableLabel("SCORE", _f_standard, true, new Point(_z_canvas.rectangle.size.w - 10, 30)), l_timer = new UpdatableLabel("TIME", _f_standard, true, new Point(_z_canvas.rectangle.size.w - 150, 30)), l_niveau = new UpdatableLabel("NIVEAU", _f_standard, true, new Point(_z_canvas.rectangle.size.w - 10, _z_canvas.rectangle.size.h - 50));
        _ul_munitions = new UpdatableLabel(0, _f_big, false, new Point(10, 60));
        _ul_timer = new UpdatableLabel(0, _f_big, true, new Point(_z_canvas.rectangle.size.w - 150, 60));
        _ul_score = new UpdatableLabel(0, _f_big, true, new Point(_z_canvas.rectangle.size.w - 10, 60));
        _ul_level = new UpdatableLabel(0, _f_big, true, new Point(_z_canvas.rectangle.size.w - 10, _z_canvas.rectangle.size.h - 20));
        _ft_alain = new FixedTarget(new Point(_z_canvas.rectangle.size.w / 2 - 60, 0), new Size(120, 90), -100, 'ressources/images/alain.png');
        _ft_chuck = new FixedTarget(new Point(10, _z_canvas.rectangle.size.h - 85), new Size(100, 85), -100, 'ressources/images/chuck.png');
        _db_alain = new DialogBox("Alain Berset:", _f_reduced, "Sauvez-moi !", _f_standard, false, new Point(370, 25));
        _db_chuck = new DialogBox("Chuck Norris:", _f_reduced, "Help me get rid of those damn ducks !", _f_standard, true, new Point(125, 535));
        _z_canvas.addItem(z_top);
        _z_canvas.addItem(z_pool);
        _z_canvas.addItem(z_bottom);
        i = 0;
        do {
            this.lanes.push(new Zone(new Rectangle(0, 90 + i * 85, _z_canvas.rectangle.size.w, 85)));
            z_pool.addItem(this.lanes[i]);
            i += 1;
        } while (i < 5);
        z_top.addItem(l_munitions);
        z_top.addItem(_ul_munitions);
        z_top.addItem(_ul_timer);
        z_top.addItem(_db_alain);
        z_top.addItem(_ft_alain);
        z_top.addItem(l_score);
        z_top.addItem(l_timer);
        z_top.addItem(_ul_score);
        z_bottom.addItem(_db_chuck);
        z_bottom.addItem(_ft_chuck);
        z_bottom.addItem(l_niveau);
        z_bottom.addItem(_ul_level);
        z_pool.setImgsrc('ressources/images/pool.png');

        i = 0;
        do {
            this.levels.push(new Level(this.lanes, z_pool));
            i += 1;
        } while (i < 10);

        // Définir le contenu des niveaux ICI !!!
        // Exemple pour les niveau 1 à 5 :
        let level = 0;
        let ammo = 4;
        let timer = 15;
        let ducks = 2;
        let sharks = 0;
        let lanes = 0;
        
        //5 lanes max, à voir pour les levels 5,6,7
        
        do{

            this.levels[level].setAmmo(ammo);
            this.levels[level].setTimer(timer);
            this.levels[level].setNbDucks(ducks);
            this.levels[level].setNbSharks(sharks);
            this.levels[level].setNbSwitchLanes(lanes);
            
            ammo  += 3;
            timer += 7;
            ducks += 4;
            sharks++;
            lanes++;
            level++;

        } while (level<7);
        
        switch(level){
            case 7:
                this.levels[7].setAmmo(19);
                this.levels[7].setTimer(60);
                this.levels[7].setNbDucks(14);
                this.levels[7].setNbSharks(0);
                this.levels[7].setNbSwitchLanes(3);
                break;

            case 8:
                this.levels[8].setAmmo(14);
                this.levels[8].setTimer(60);
                this.levels[8].setNbDucks(16);
                this.levels[8].setNbSharks(4);
                this.levels[8].setNbSwitchLanes(4);
                break;
                
            case 9:
                this.levels[9].setAmmo(30);
                this.levels[9].setTimer(60);
                this.levels[9].setNbDucks(30);
                this.levels[9].setNbSharks(10);
                this.levels[9].setNbSwitchLanes(5);
                
            }
        
//        ...

        _z_canvas.draw();
        this.playing = true;
        this.levels[0].start(this);
    };
    this.nextLevel = function () {
        let currLevel = _ul_level.value - 1;
        if (!this.levels[currLevel].finished) {
            console.log("game over");
            this.playing = false;
            showHallOfFame();
        } else if (currLevel === this.levels.length - 1) {
            console.log("game finished");
            this.playing = false;

            // End of the game
            this.endGame = function(){
                // Hide everything
                _modal.style.display = "none";
                _canvas.style.display = "none";

                // Get the three divs
                let egDiv = document.getElementById("endGame");
                let egTitleBox = document.getElementById("egTitleBox");
                let egImgs = document.getElementById("egImgs");

                // Create a title
                let header = document.createElement("h1");
                header.style.display = "block";
                header.style.height = "20 px";
                header.innerHTML = "Help Alain by opening his cell !";
                egTitleBox.appendChild(header);

                // Create key img
                let key = document.createElement("img");
                key.src = "ressources/images/key.png";
                key.setAttribute("width", "20%");
                key.setAttribute("float", "left");
                key.setAttribute("display", "inline");
                key.setAttribute('draggable', true);
            
                // Create Alain img
                let alainJail = document.createElement("img");
                alainJail.src = "ressources/images/alain.png";
                alainJail.setAttribute("width", "20%");
                alainJail.setAttribute("float", "right");
                alainJail.setAttribute("display", "inline");

                // Allow drag n drop
                alainJail.ondragover = function(e){
                    e.preventDefault();
                }

                // On drop, modify the picture
                alainJail.ondrop = function(e){
                    e.preventDefault();
                    this.src = "ressources/images/alain_merciPatron.png";
                    egImgs.removeChild(alainJail);
                    egImgs.removeChild(key);
                    egImgs.appendChild(alainJail);

                    header.innerHTML = "Yay !! Thx man !";

                    // Show hall of fame after 2 seconds
                    setTimeout(showHallOfFame, 2000);
                }

                // Insert the imgs
                egImgs.appendChild(alainJail);
                egImgs.appendChild(key);

                // Show the egDiv
                egDiv.style.display = "block";
                egTitleBox.style.display = "block";
                egImgs.style.display = "flex";
                egImgs.style.justifyContent = "center";
            }

            this.endGame();
        } else {
            this.levels[currLevel + 1].start(this);
        }
    };
    this.shoot = function (point) {
        this.levels[_ul_level.value - 1].shoot(point);
    };
}

function initCanvas() {
    _modal = document.getElementById('myModal');
    _hallOfFame = document.getElementById('hallOfFame')
	_canvas = document.getElementById('piscine');
    if (_canvas === null) {
        document.getElementById('game-errmsg').style.visibility = 'visible';
        return;
    } else if (window.getComputedStyle(_canvas).getPropertyValue('visibility').toString() !== 'visible') {
        document.getElementById('game-warmsg').style.visibility = 'visible';
        return;
    }
	_canvas.addEventListener('mousedown', function (e) {
		getCursorPosition(e);
	});
    document.getElementById("submitModal").addEventListener('click', function () {
        closeModal();
    });

    document.getElementById("skipbtn").addEventListener('click', function() {
        skipVideo();
    });
	_ctx = _canvas.getContext("2d");
    _z_canvas = new Zone(new Rectangle(0, 0, _canvas.getAttribute("width"), _canvas.getAttribute("height")));
    _f_standard = new Font("Arial", 20);
    _f_big = new Font("Arial", 24);
    _f_reduced = new Font("Arial", 18);
    _game = new Game();
    
    hideHallOfFame();
    showModal();
}

function skipVideo(){
    if(_videoTO !== null){
        clearTimeout(_videoTO);
        clearTimeout(_startOriginal);
        _video.pause();
        finalCountdown(3);

        setTimeout(function () {
            startGame();
        }, 5000);
    }
}

window.onload = initCanvas;
