"use strict";
function hamburgerClicked() {
    let lis = document.querySelectorAll('nav>ul>li:not(.hamburger)');
    if (lis.length > 0) {
        let newprop = 'none';
        if (window.getComputedStyle(lis[0]).getPropertyValue('display').toString() === 'none') {
            newprop = 'table-cell';
        }
        for (let i = 0; i < lis.length; i++) {
            lis[i].style.display = newprop;
        }
    }
}

function findHamburger() {
    let hamburgers = document.getElementsByClassName('hamburger');
    if (hamburgers.length === 1) {
        let hamburger = hamburgers[0];
        hamburger.addEventListener('click', function () {
            hamburgerClicked();
        });
    }
}

document.addEventListener("DOMContentLoaded", function() {
    findHamburger();
});